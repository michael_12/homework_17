#include <iostream>

class Vector
{
public:
	Vector(): x (10), y(15), z(12)
	{}
	Vector(double _x, double _y, double _z):x(_x),y(_y),z(_z)
	{}
	void Show()
	{
		std::cout << '\n' << x << "  " << y << "  " << z;
	}
	double vector(double a, double b, double c)
	{
		return sqrt(pow(a, 2) + pow(b, 2) + pow(c, 2));
	}
private:
	double x;
	double y;
	double z;
	
};

int main()
{
	Vector v;
	v.Show();
}